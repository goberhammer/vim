#!/bin/bash

echo "Vim Setup script"
echo "================"

echo "Installing Vim Jonathon PPA"
sudo apt-get install fonts-hack-ttf
#sudo add-apt-repository ppa:jonathonf/vim && sudo apt-get update && sudo apt-get install vim-gtk3
sudo apt-get update && sudo apt-get install vim-gtk3

echo "Installing support packages..."
sudo apt-get install universal-ctags
sudo apt-get install silversearcher-ag
sudo apt-get install git

echo "Installing Vim Plug..."
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "Installing hack font..."
sudo apt-get install fonts-hack-ttf
if [ $? -ne 0 ]; then
    echo "Package not found, falling back to local copy..."
    sudo dpkg -i misc/fonts-hack-ttf_2.020-1_all.deb
fi

echo "Insatalling FZF..."
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install

echo ""
echo "Done, exec :PlugInstall in vim to install al plugins and:"
echo "  - markdown-preview: call mkdp#util#install()"
echo "Bye :-)"
echo ""

