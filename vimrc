" Vundle ===============================================================
set nocompatible              " be iMproved, required
filetype off                  " required

call plug#begin('~/.vim/plugged')
" Unite
"Plug 'Shougo/unite.vim'
"Plug 'Shougo/vimproc.vim'
" Vinarise (hex editor)
Plug 'Shougo/vinarise.vim'
" Vim signature
Plug 'kshenoy/vim-signature'
" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Qf
Plug 'romainl/vim-qf'
" CSV
"Plug 'chrisbra/csv.vim'
" Targets
Plug 'wellle/targets.vim'
" Ack Vim
Plug 'mileszs/ack.vim'
" Changes - serve VIM8
"Plug 'chrisbra/changesPlugin'
" Tabular
Plug 'godlygeek/tabular'
" Markdown
Plug 'preservim/vim-markdown'
" Markdown preview
Plug 'iamcco/markdown-preview.nvim'
" T.Pope
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-repeat'
" IncSearch
if has("patch-8.0.1238")
    " TODO: include is.vim ?
else
    Plug 'haya14busa/incsearch.vim'
endif
" Srecord
"Plug 'editsrec'
" Kana textobj user
Plug 'kana/vim-textobj-user'
" Idbrii word column
Plug 'idbrii/textobj-word-column.vim'
" muComplete
Plug 'lifepillar/vim-mucomplete'
" Clang-complete
"if filereadable("/usr/lib/llvm-8/lib/libclang.so.1")
"    Plug 'Rip-Rip/clang_complete'
"endif
" Tagbar
Plug 'majutsushi/tagbar'
" GutenTags
if v:version > 800
    Plug 'ludovicchabant/vim-gutentags'
endif
" FZF
set rtp+=~/.fzf
Plug 'junegunn/fzf.vim'
" CtrlXA enhancements
Plug 'konfekt/vim-ctrlxa'
" Syntastic
Plug 'vim-syntastic/syntastic'
" Jedi (python complete)
"Plug 'davidhalter/jedi-vim'
call plug#end()


if v:version > 800
    packadd termdebug
endif

" Temporary disable modlines
set modelines=0
set nomodeline

" set guioptions=aegimrLtTb
"set guioptions=aegimrLtb
"set guioptions+=b
set guioptions=aAeimgtrLbh
set guifont=Hack\ 11

if v:version > 800
    set shortmess+=c
endif

set showtabline=3
set tabpagemax=30

set nowrap
set wrapmargin=0
set textwidth=72
set autoread
set tabstop=4
set expandtab
set shiftwidth=4
set softtabstop=4
set noswapfile
set nobackup
set showmatch        " set show matching parenthesis
set ignorecase       " ignore case when searching
set smartcase        " ignore case if search pattern is all lowercase,
                     "    case-sensitive otherwise
set visualbell       " Disattiva il cazzo di campanello...
set incsearch
set hlsearch
set hidden

"set scrolloff=3     " Minimal number of screen lines to keep above and below the cursor
set scrolljump=1     " How many lines to scroll at a time, make scrolling appears faster
set colorcolumn=+1   " Column width indicator
hi ColorColumn ctermbg=lightgrey guibg=yellow guifg=blue

set splitright
set splitbelow

" Show tabs and other whitespace
set listchars=tab:>-,trail:~,extends:>,precedes:<
set list

" Visualizza linee selezionate
set showcmd

" Tab label
set guitablabel=%M\ %t

" set nowritebackup

" See also autocommand below
set number relativenumber

set nostartofline

set cmdheight=2

" Always show sign column
set signcolumn=yes

colorscheme wombat_1

if has('gui_running')
    winpos 400 70
    set lines=56
    set columns=170
else
endif

syn on
set mousemodel=popup_setpos
" Attiva la numerazione delle linee
set nu

" Completamento sopra command line
set wildmenu
set wildignore+=*.o,*.obj,*.d,*.pyc

" This turns off physical line wrapping (ie: automatic insertion of newlines)
set formatoptions-=t

" Fix join linee commentate
if v:version > 703 || v:version == 703 && has('patch541')
    set formatoptions+=j
endif

" Cursor line
set cursorline

" Popup completamento
"set completeopt=menu,menuone,preview
"set completeopt=longest,menuone,preview
" Evidenzia popup menu di omnicomplete
" TODO: spostare nel tema wombat_1
:highlight Pmenu guibg=DimGrey
:highlight PmenuSel guibg=LightGreen gui=bold

" Cerca i tags nella directory corrente e in quelle superiori
set tags=./tags;/
" Visualizza stato GutenTags
set statusline+=%{gutentags#statusline()}
let g:gutentags_ctags_extra_args = [ '--c-kinds=+mdefgpstux', '-R', '--fields=+KS' ]
"let g:gutentags_ctags_extra_args = ['--fields=+niazS', '--extra=+q']
"let g:gutentags_ctags_extra_args += ['--c++-kinds=+px']
"let g:gutentags_ctags_extra_args += ['--c-kinds=+px']
"let g:gutentags_ctags_extra_args += ['--output-format=e-ctags']
" Debug
"let g:gutentags_trace = 1

" Visualizza airline sempre
set laststatus=2
" Tema airline
let g:airline_theme='cool'
"let g:airline_theme = 'wombat'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#tab_nr_type = 0
let g:airline#extensions#tabline#buffer_nr_show = 1
let g:airline#extensions#tabline#show_buffers = 2
let g:airline#extensions#tabline#show_splits = 1
let g:airline#extensions#tabline#formatter = 'unique_tail'
"let g:airline#extensions#tabline#excludes = ['qf']
let g:airline#extensions#csv#enabled = 1
let g:airline_detect_modified = 1
let g:airline_detect_paste = 1
let g:airline_detect_crypt = 1
let g:airline_detect_spell = 1
let g:airline_detect_spelllang = 1
" W/a column symbol not showing correclty
if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
let g:airline_symbols.colnr = '㏇'

" clang_complete
let g:clang_library_path='/usr/lib/llvm-8/lib/libclang.so.1'
let g:clang_user_options = '-std=c++14'
let g:clang_complete_auto = 1

" YCM
"let g:ycm_collect_identifiers_from_tags_files = 1
"let g:ycm_add_preview_to_completeopt = 1
"let g:ycm_autoclose_preview_window_after_completion = 1
"let g:ycm_autoclose_preview_window_after_insertion = 1

" Omni complete
set completeopt-=preview
set completeopt-=t
"set completeopt-=longest
set completeopt+=longest
set completeopt+=menuone
if v:version > 800
    set completeopt+=noselect
endif
" muComplete
let g:mucomplete#enable_auto_at_startup = 0
imap <c-space> <plug>(MUcompleteFwd)
imap <c-s-space> <plug>(MUcompleteBwd)
let g:mucomplete#cycle_with_trigger = 1
let g:mucomplete#chains = {}
""let g:mucomplete#chains.default = ['file', 'omni', 'keyn', 'dict', 'ulti']
"let g:mucomplete#chains.default = [ 'path', 'omni', 'tags', 'incl', 'keyn', 'dict', 'uspl']
let g:mucomplete#chains.default = [ 'path', 'omni', 'tags', 'incl', 'dict', 'uspl']
"let g:mucomplete#chains.unite = []
":MUcompleteNotify 2
" Jedi
"let g:jedi#auto_initialization = 0
"let g:jedi#auto_vim_configuration = 0
let g:jedi#force_py_version = 3
let g:jedi#popup_on_dot = 0  " It may be 1 as well
let g:jedi#show_call_signatures = "2"

" Netrw
" Tree view
let g:netrw_altv = 1
let g:netrw_liststyle = 3
let g:netrw_banner = 0
" Open in previous window
let g:netrw_browse_split = 4
let g:netrw_winsize = 10

" Changes plugin
:let g:changes_vcs_check=1
:let g:changes_sign_text_utf8=1

" FZF
" Always enable preview window on the right with 60% width
let g:fzf_preview_window = 'right:40%'

" Syntastic
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_strikethrough = 1

" Autocommands
" Force markdown on .md
autocmd BufNewFile,BufReadPost *.md set filetype=markdown
" Force srec on .mhx
autocmd BufNewFile,BufReadPost *.mhx set filetype=srec
" Force CSS on .qss
autocmd BufNewFile,BufReadPost *.qss set filetype=css
" Force XML on .qrc
autocmd BufNewFile,BufReadPost *.qrc set filetype=xml
" Close preview window after completion
autocmd CompleteDone * pclose
" Automatic relative numbers
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END
" For completing keywords with underscore
autocmd Filetype c setlocal iskeyword+=_

" Use Ag instead of Ack
if executable('ag')
    if v:version < 801
        let g:ackprg = 'ag --column'
    else
        let g:ackprg = 'ag --vimgrep'
    endif
endif
" Autofold search result per file
let g:ack_autofold_results = 1

" Functions
" Show highlight group for word under cursor
nmap <C-_> :call <SID>SynStack()<CR>
function! <SID>SynStack()
  if !exists("*synstack")
    return
  endif
  echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
endfunc


" Remaps

" change the mapleader from '\' to ' '
"let mapleader=","
map <SPACE> <leader>

" Insert mode exit
inoremap jf <esc>
" No highlight
map <Leader>hn :noh<CR>
" Highlight whitespace
map <Leader>hw :g/\s\+$<CR>

" Keep selected text selected when fixing indentation
vnoremap < <gv
vnoremap > >gv

" Find
"map <Leader>fc :Ack! -f --cc --cpp -w <cword><CR>
"map <Leader>fh :Ack! -f --hh -w <cword><CR>
"map <Leader>fa :Ack! -f -w <cword><CR>
"map <Leader>cc :cclose<CR>
"map <Leader>cn :cnext<CR>
"map <Leader>cp :cprev<CR>
command! -nargs=+ -complete=file AGC call fzf#vim#ag_raw(<q-args>)
map <Leader>ff :Files<CR>
"map <Leader>fa :Ag<CR>
nnoremap <silent> <Leader>fa :Ag <C-R><C-W><CR>
nnoremap <silent> <Leader>fc :AGC --cc --cpp <C-R><C-W><CR>
map <Leader>ft :call fzf#vim#tags(expand('<cword>'))
map <Leader>fT :Tags<CR>
map <Leader>fG :GFiles<CR>
map <Leader>fH :History<CR>
map <Leader>fC :Commits<CR>

" Misc
"map <Leader>lc <Esc>I//<Esc>p
" Enclose visual selection in #if 0
map <Leader>si yO#ifdef 0<Esc>'>o#else<Esc>o#endif<Esc>O

" Disabilita frecce e esc
noremap <up> <nop>
noremap <down> <nop>
noremap <left> <nop>
noremap <right> <nop>
"inoremap <esc> <nop>

" MiniBufExpl
"map <Leader>mo :MBEOpen<CR>
""map <Leader>mc :MBEClose<cr>
""map <Leader>mt :MBEToggle<cr>
"map <Leader>md :MBEbd<CR>
""noremap <TAB>   :MBEbn<CR>
""noremap <S-TAB> :MBEbp<CR>

" AirLine
noremap <TAB>   :bn<CR>
noremap <S-TAB> :bp<CR>
noremap <C-TAB>   :tabn<CR>
noremap <C-S-TAB> :tabp<CR>

" Omnicomplete
":inoremap <C-space> <c-x><c-o>
":inoremap <C-space> <c-x><c-o><c-r>=pumvisible() ? "\<lt>Down>" : ""<CR>
" Simulates <C-X><C-O> to bring up the omni completion menu, then it
" simulates <C-N><C-P> to remove the longest common text, and finally it
" simulates <Down> again to keep a match highlighted.
"inoremap <expr> <C-space> pumvisible() ? '<C-n>' :
"  \ '<C-x><C-o><C-n><C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

" Evita newline se si seleziona la prima riga di un compl. menu
:inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

":inoremap <expr> <C-n> pumvisible() ? '<C-n>' :
"  \ '<C-n><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'
":inoremap <expr> <C-p> pumvisible() ? '<C-p>' :
"  \ '<C-p><C-r>=pumvisible() ? "\<lt>Down>" : ""<CR>'

" Open omni completion menu closing previous if open and opening new
" menu without changing the text
":inoremap <expr> <C-Space> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
"            \ '<C-x><C-o><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>" : ""<CR>'
"" Open user completion menu closing previous if open and opening new
"" menu without changing the text
":inoremap <expr> <S-Space> (pumvisible() ? (col('.') > 1 ? '<Esc>i<Right>' : '<Esc>i') : '') .
"            \ '<C-x><C-u><C-r>=pumvisible() ? "\<lt>C-n>\<lt>C-p>\<lt>Down>" : ""<CR>'

" IncSearch
if has("patch-8.0.1238")
else
    map /  <Plug>(incsearch-forward)
    map ?  <Plug>(incsearch-backward)
    map g/ <Plug>(incsearch-stay)
    augroup incsearch-keymap
        autocmd!
        autocmd VimEnter * call s:incsearch_keymap()
    augroup END
    function! s:incsearch_keymap()
        IncSearchNoreMap <C-g> <Over>(incsearch-next)
        IncSearchNoreMap <C-t> <Over>(incsearch-prev)
        IncSearchNoreMap <C-f> <Over>(incsearch-scroll-f)
        IncSearchNoreMap <C-b> <Over>(incsearch-scroll-b)
    endfunction
endif

